﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MCU4Mobile
{
    [Activity(Label = "PinAccept", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class PinAccept : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            RequestWindowFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.PinControl);

            Button btnPinOK = FindViewById<Button>(Resource.Id.btnPinOK);
            btnPinOK.Click += delegate
            {
                Intent page = new Intent(this, typeof(AudioMaster));
                StartActivity(page);
                Finish();
            };
        }
    }
}