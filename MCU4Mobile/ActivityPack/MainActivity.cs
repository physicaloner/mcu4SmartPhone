﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MCU4Mobile
{
    [Activity(Label = "MCU4Mobile", MainLauncher = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            RequestWindowFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.Home);

            Button btnLogin = FindViewById<Button>(Resource.Id.btnLogin);
            btnLogin.Click += delegate
            {      
                Intent page = new Intent(this, typeof(LoginMenu));
                StartActivity(page);
                Finish();
            };

            Button btnLoginAd = FindViewById<Button>(Resource.Id.btnLoginAd);
            btnLoginAd.Click += delegate
            {
                DataController.ConnectToServer();

                Intent page = new Intent(this, typeof(AudioMaster));
                StartActivity(page);
                Finish();
            };
        }
    }
}

