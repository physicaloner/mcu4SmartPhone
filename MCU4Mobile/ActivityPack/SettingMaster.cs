﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MCU4Mobile
{
    [Activity(Label = "SettingMaster", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class SettingMaster : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            RequestWindowFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.Setting);

            ImageButton imgMic = FindViewById<ImageButton>(Resource.Id.image_1_s);
            imgMic.Click += delegate
            {
                Intent page = new Intent(this, typeof(AudioMaster));
                StartActivity(page);
                Finish();
            };

            ImageButton imgMain = FindViewById<ImageButton>(Resource.Id.image_2_s);
            imgMain.Click += delegate
            {
                Intent page = new Intent(this, typeof(MainMaster));
                StartActivity(page);
                Finish();
            };
        }
    }
}