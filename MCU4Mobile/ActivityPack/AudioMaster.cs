﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Vision;
using Android.Gms.Vision.Texts;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Speech;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace MCU4Mobile
{
    [Activity(Label = "AudioMaster", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, Theme = "@style/Theme.AppCompat.NoActionBar")]
    public class AudioMaster : AppCompatActivity
    {
        const int VOICE = 1987;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            RequestWindowFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.Audio);

            Bitmap bitmap = BitmapFactory.DecodeResource(ApplicationContext.Resources, Resource.Drawable.microphone);

            ImageButton imgMain = FindViewById<ImageButton>(Resource.Id.image_2_a);
            imgMain.Click += delegate
            {
                Intent page = new Intent(this, typeof(MainMaster));
                StartActivity(page);
                Finish();
            };

            ImageButton imgSetting = FindViewById<ImageButton>(Resource.Id.image_3_a);
            imgSetting.Click += delegate
            {
                Intent page = new Intent(this, typeof(SettingMaster));
                StartActivity(page);
                Finish();
            };

            ImageButton imgAudio = FindViewById<ImageButton>(Resource.Id.imageMic);
            imgAudio.Click += delegate
            {
                string rec = Android.Content.PM.PackageManager.FeatureMicrophone;
                if (rec != "android.hardware.microphone")
                {
                    var alert = new Android.Support.V7.App.AlertDialog.Builder(imgAudio.Context);
                    alert.SetTitle("คุณไม่ได้อนุญาตให้แอพพลิเคชั่นเข้าถึงไมโครโฟนของคุณ !");
                    alert.SetPositiveButton("ตกลง", (sender, e) =>
                    {
                        return;
                    });
                    alert.Show();
                }
                else
                {
                    var voiceIntent = new Intent(RecognizerIntent.ActionRecognizeSpeech);
                    voiceIntent.PutExtra(RecognizerIntent.ExtraLanguageModel, RecognizerIntent.LanguageModelFreeForm);
                    voiceIntent.PutExtra(RecognizerIntent.ExtraPrompt, "พูดอะไรบางอย่าง");
                    voiceIntent.PutExtra(RecognizerIntent.ExtraSpeechInputCompleteSilenceLengthMillis, 1500);
                    voiceIntent.PutExtra(RecognizerIntent.ExtraSpeechInputPossiblyCompleteSilenceLengthMillis, 1500);
                    voiceIntent.PutExtra(RecognizerIntent.ExtraSpeechInputMinimumLengthMillis, 15000);
                    voiceIntent.PutExtra(RecognizerIntent.ExtraMaxResults, 1);
                    voiceIntent.PutExtra(RecognizerIntent.ExtraLanguage, "en-US");
                    StartActivityForResult(voiceIntent, VOICE);
                }

                //TextRecognizer recognizer = new TextRecognizer.Builder(ApplicationContext).Build();
                //if (recognizer.IsOperational)
                //{
                //    Log.Error("ERROR", "Google play service deny !");
                //}
                //else
                //{
                //    Frame frame = new Frame.Builder().SetBitmap(bitmap).Build();
                //    SparseArray items = recognizer.Detect(frame);
                //    StringBuilder stringBuilder = new StringBuilder();

                //    for (int i = 0; i < items.Size(); i++)
                //    {
                //        TextBlock item = (TextBlock)items.ValueAt(i);
                //        stringBuilder.Append(item.Value);
                //        stringBuilder.Append("\r\n");
                //    }

                //    // Send data to arduino
                //    DataController.SendToServer(stringBuilder.ToString());
                //}
            };
        }

        protected override void OnActivityResult(int requestCode, Result resultVal, Intent data)
        {
            if (requestCode == VOICE)
            {
                if (resultVal == Result.Ok)
                {
                    var matches = data.GetStringArrayListExtra(RecognizerIntent.ExtraResults);
                    if (matches.Count != 0)
                    {
                        string textInput = matches[0];
                        Toast.MakeText(this, "Say : " + textInput, ToastLength.Long).Show();
                        System.Diagnostics.Debug.WriteLine("Speech text : " + textInput);
                        apiConnection.SenderMCU(textInput);
                    }
                    else
                    {
                        ImageButton imgAudio = FindViewById<ImageButton>(Resource.Id.imageMic);
                        var alert = new Android.Support.V7.App.AlertDialog.Builder(imgAudio.Context);
                        alert.SetTitle("No speech was recognised");
                        alert.SetPositiveButton("OK", (sender, e) =>
                        {
                            return;
                        });
                        alert.Show();

                        Toast.MakeText(this, "Not found this text !", ToastLength.Long).Show();
                    }
                }
                base.OnActivityResult(requestCode, resultVal, data);
            }
        }
    }
}