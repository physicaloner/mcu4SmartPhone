﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MCU4Mobile
{
    [Activity(Label = "LoginMenu", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class LoginMenu : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            RequestWindowFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.HomeMenu);

            EditText txtUser = FindViewById<EditText>(Resource.Id.txtUsername_m);
            EditText txtPwd = FindViewById<EditText>(Resource.Id.txtPassword_m);

            Button btnLogin = FindViewById<Button>(Resource.Id.btnLogin_m);
            btnLogin.Click += delegate
            {
                var statusOk = apiConnection.RequestUserID(txtUser.Text, txtPwd.Text, this);
                if (statusOk)
                {
                    Toast.MakeText(this, "Log in status is OK", ToastLength.Long).Show();
                    //DataController.ConnectToServer();
                    Intent page = new Intent(this, typeof(MainMaster));
                    StartActivity(page);
                    Finish();
                }
            };
        }
    }
}