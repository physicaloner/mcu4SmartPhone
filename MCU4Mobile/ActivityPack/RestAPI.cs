﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace MCU4Mobile.ActivityPack
{
    class RestAPI : IDisposable
    {
        HttpClient _client;

        public RestAPI()
        {
            _client = new HttpClient();
            _client.MaxResponseContentBufferSize = 256000;
            _client.Timeout = TimeSpan.FromSeconds(1);
            _client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded' "));
        }

        public async Task<T> GetResponse<T>(string url) where T : class
        {
            var response = await _client.GetAsync(url);
            var jsonResult = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<T>(jsonResult);
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}