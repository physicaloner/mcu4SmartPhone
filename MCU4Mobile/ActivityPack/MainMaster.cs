﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MCU4Mobile
{
    [Activity(Label = "MainMenu", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainMaster : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            RequestWindowFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.Main);

            ImageButton imgMic = FindViewById<ImageButton>(Resource.Id.image_1_m);
            imgMic.Click += delegate
            {
                Intent page = new Intent(this, typeof(AudioMaster));
                StartActivity(page);
                Finish();
            };

            ImageButton imgSetting = FindViewById<ImageButton>(Resource.Id.image_3_m);
            imgSetting.Click += delegate
            {
                Intent page = new Intent(this, typeof(SettingMaster));
                StartActivity(page);
                Finish();
            };

            Button btnSwitch = FindViewById<Button>(Resource.Id.btnSwitch);
            btnSwitch.Click += delegate
            {
                Intent page = new Intent(this, typeof(PinAccept));
                StartActivity(page);
                Finish();
            };
        }
    }
}