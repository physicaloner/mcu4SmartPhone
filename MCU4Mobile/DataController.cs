﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using Debug = System.Diagnostics.Debug;

namespace MCU4Mobile
{
    public class BufferObject
    {
        public Socket workSocket = null;  
        public const int BufferSize = 1024;
        public byte[] buffer = new byte[BufferSize]; 
        public StringBuilder sb = new StringBuilder();      
    }

    class DataController
    {
        private const int port = 1150;
        private static ManualResetEvent connectDone = new ManualResetEvent(false);
        private static String response = String.Empty;
        private static Socket client;

        public static void ConnectToServer()
        { 
            try
            {  
                // Custom Server IP from No-ip :
                /* IPHostEntry ipHostInfo = Dns.GetHostEntry("arduino.no-ip.com");
                IPAddress ipAddress = ipHostInfo.AddressList[0]; */
                // Custom Server IP from Lan :
                IPAddress ipAddress = IPAddress.Parse("172.20.10.1");
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                Socket client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp); 
                client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), client);
                connectDone.WaitOne();

                SendToServer("Mobile Connected.<EOF>");
                OnReceiver(client);

                Debug.WriteLine("Response received : {0}", response);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        private static void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;
                client.EndConnect(ar);
                Debug.WriteLine("Connected to Arduino IP : {0}", client.RemoteEndPoint.ToString());
                connectDone.Set();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        private static void OnReceiver(Socket client)
        {
            try
            { 
                BufferObject state = new BufferObject();
                state.workSocket = client;                
                client.BeginReceive(state.buffer, 0, BufferObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        private static void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                BufferObject state = (BufferObject)ar.AsyncState;
                Socket client = state.workSocket;

                int bytesRead = client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
                    client.BeginReceive(state.buffer, 0, BufferObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);

                    response = state.sb.ToString();
                    if (response.IndexOf("<EOF>") > -1)
                    {
                        Debug.WriteLine("Read {0} bytes from socket. \n Data : {1}", response.Length, response);
                        SendToServer(response);
                    }
                    else
                    {
                        client.BeginReceive(state.buffer, 0, BufferObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        public static void SendToServer(String data)
        {
            try
            {
                byte[] byteData = Encoding.ASCII.GetBytes(data);
                client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), client);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;
                int bytesSent = client.EndSend(ar);
                Debug.WriteLine("Sent {0} bytes to server.", bytesSent);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }
    }
}