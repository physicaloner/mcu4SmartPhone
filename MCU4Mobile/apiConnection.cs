﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ModernHttpClient;
using Newtonsoft.Json;

namespace MCU4Mobile
{
    class apiConnection
    {
        public static string UserId { get; set; }
        public static string UserPwd { get; set; }
        public static string TmpPwd { get; set; }

        public static string apiServer = "http://172.20.10.2:5000";
        public static string mcuServer = "http://192.168.43.231";

        public static bool RequestUserID(string usr, string pwd, Context _context)
        {
            using (HttpClient client = new HttpClient(new NativeMessageHandler()))
            {
                client.Timeout = TimeSpan.FromSeconds(3);

                try
                {
                    // var result = await client.GetAsync(@"http://172.20.10.2:5000/api/user/" + usr + "/" + pwd);
                    var requestTask = client.GetAsync(@"" + apiServer + "/api/user/" + usr + "/" + pwd); var response = Task.Run(() => requestTask).Result;

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var user = JsonConvert.DeserializeObject<dynamic>(Task.Run(() => response.Content.ReadAsStringAsync()).Result);

                        UserId = user.usrid;
                        UserPwd = user.usrpwd;
                        TmpPwd = user.tmppwd;

                        System.Diagnostics.Debug.WriteLine(Task.Run(() => response.Content.ReadAsStringAsync()).Result);

                        return true;
                    }
                    else
                    {
                        Toast.MakeText(_context, "Cannot connect to server.", ToastLength.Long);
                        System.Diagnostics.Debug.WriteLine("No content.");

                        return false;
                    }
                }
                catch (Exception e)
                {
                    Toast.MakeText(_context, "Cannot connect to server.", ToastLength.Long);
                    System.Diagnostics.Debug.WriteLine("No content.");

                    return false;
                }
            }
        }

        public static bool SenderMCU(string msg)
        {
            using (HttpClient client = new HttpClient(new NativeMessageHandler()))
            {
                //var result = await client.GetAsync(@"http://172.20.10.5/" + msg);
                var requestTask = client.GetAsync(mcuServer + "/" + msg); var response = Task.Run(() => requestTask).Result;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    System.Diagnostics.Debug.WriteLine("OK.");
                    return true;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("No content.");
                    return false;
                }
            }
        }

    }
}